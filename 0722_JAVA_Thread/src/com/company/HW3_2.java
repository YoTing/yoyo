package com.company;

public class HW3_2 {
    static Count count = new Count();
    public static void main(String[] args){
        thread th1 = new thread();
        thread th2 = new thread();

        th1.start();
        th2.start();
    }

    static class Count{
        int n;
    }

    static class thread extends Thread{
        @Override
        public void run() {
            synchronized (HW3_2.count) {
                for (int i = 0; i < 600; i++) {
                    HW3_2.count.n += 1;
                    System.out.println(this.getName() + ":" + count.n);
                }
            }
        }
    }
}
