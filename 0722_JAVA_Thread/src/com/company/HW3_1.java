package com.company;

public class HW3_1 {
    static int n = 0;
    public static void main(String[] args){
        th th1 = new th();
        th th2 = new th();

        th1.start();
        th2.start();
    }

    public static class th extends Thread{
        @Override
        public void run() {
            for (int i = 0; i < 600; i++){
                HW3_1.n += 1;
                System.out.println(this.getName() + ":" + n);
            }
        }
    }
}
