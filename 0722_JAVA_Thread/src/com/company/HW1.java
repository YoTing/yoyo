package com.company;

import java.util.Scanner;

public class HW1 {
    static String in="start";
    public static void main(String[] args){

        Scanner input=new Scanner(System.in);
        int num = (int) (Math.random()*100)+1;
        System.out.println("num:"+num);

        time1 t1=new time1();
        t1.start();

        int max = 100, min =0;
        while (true) {
            System.out.println("Input the number:");
            int guess = input.nextInt();
            if (guess <= 100 && guess >= 1) {
                if (guess > num) {
                    max = guess-1;
                    System.out.println("猜太大了。" + min + "~" + max);
                    t1.answer(guess,num);
                } else if (guess < num) {
                    min = guess+1;
                    System.out.println("猜太小了。" + min + "~" + max);
                    t1.answer(guess,num);
                } else if (guess == num) {
                    System.out.println("猜中了。");
                    t1.answer(guess,num);
                    break;
                }
            } else {
                System.out.println("範圍錯誤");
            }
        }
    }

    public static class time1 extends Thread{
        int second=0;
        boolean ans;
        public boolean answer(int g,int n){
            if (g == n){
                ans = true;
                return ans;
            }
            ans = false;
            return ans;
        }
        public void run(){
            while (true) {
                if (ans != true) {
                    try {
                        sleep(1000);
                        second++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else if (ans == true){
                    System.out.println("你猜了:" + second + "s");
                    break;
                }
            }
        }
    }
}
