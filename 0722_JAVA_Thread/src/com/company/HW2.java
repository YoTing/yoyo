package com.company;

import java.util.Calendar;
import java.util.Scanner;

public class HW2 {
    static String order = "1";
    public static void main(String[] args){

        Scanner input=new Scanner(System.in);
        time2 t2=new time2();
        t2.start();
        while (true){
            order = input.next();
        }
    }

    public static class time2 extends Thread{

        public void run(){
            Calendar cal = Calendar.getInstance();
            while (true) {
                if ( order.equals("stop")){
                    System.out.println("已停止");
                    break;
                }else {
                    cal.setTimeInMillis(System.currentTimeMillis());
                    int hour = cal.get(Calendar.HOUR_OF_DAY);
                    int min = cal.get(Calendar.MINUTE);
                    int sec = cal.get(Calendar.SECOND);
                    int change = Integer.valueOf(order);
                    try {
                        System.out.println("當前時間: " + hour + "時" + min + "分" + sec + "秒");
                        sleep(change * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
