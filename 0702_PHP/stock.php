<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>書店-目前庫存</title>
</head>
<body>
<form method="get" action="">
<center>
<?php
	$link= @mysqli_connect(	//開啟資料庫
		'localhost','root','','bookstore_0702');
		
	echo "<p><b>庫存內容如下(依數量排序):</b>";
	
	echo "<table border=2><tr>";
	echo "<td>書號</td>";
	echo "<td>書名</td>";
	echo "<td>數量</td>";
	echo "<td>最新進貨時間</td>";
	echo "</tr>";
	
	$sql="SELECT * FROM `目前庫存` ORDER BY `目前庫存`.`數量` ASC";	
	mysqli_query($link,'SET NAMES utf8');
	$result=mysqli_query($link,$sql);
	$total_fields=mysqli_num_fields($result);
	
	while($row=mysqli_fetch_row($result)){
		echo "<tr>";
		for($i=0;$i<=$total_fields-1;$i++){
			echo "<td>$row[$i]</td>";
		}	
		echo "</tr>";
	}
	echo "</table><p>";
	
	echo "<p>";
	
	echo "<p><b>庫存內容如下(依日期排序):</b>";
	
	echo "<table border=2><tr>";
	echo "<td>書號</td>";
	echo "<td>書名</td>";
	echo "<td>數量</td>";
	echo "<td>最新進貨時間</td>";
	echo "</tr>";
	
	$sql="SELECT * FROM `目前庫存` ORDER BY `目前庫存`.`UPTIME` DESC";	
	mysqli_query($link,'SET NAMES utf8');
	$result=mysqli_query($link,$sql);
	$total_fields=mysqli_num_fields($result);
	
	while($row=mysqli_fetch_row($result)){
		echo "<tr>";
		for($i=0;$i<=$total_fields-1;$i++){
			echo "<td>$row[$i]</td>";
		}	
		echo "</tr>";
	}
	echo "</table>";
	
	mysqli_free_result($result);
	
	mysqli_close($link);	//關閉資料庫
	
?>
</center>
</form>
</body>
</html>