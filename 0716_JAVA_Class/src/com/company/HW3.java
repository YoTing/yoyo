package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class HW3 {
    //使用自訂類別，利用HashMap的方式，把四個地點的位置經緯度紀錄
    public static void main(String[] args) {
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("東京迪士尼","35°37N,139°52E");
        hashMap.put("台北101","25°02N,121°33E");
        hashMap.put("法國艾菲爾鐵塔","48°51N,2°17E");
        hashMap.put("美國黃石公園","44°36N,110°30E");

        Scanner input = new Scanner(System.in);
        System.out.println("Input two place:");
        String place1 = input.next();
        String place2 = input.next();

        Col co =new Col(hashMap.get(place1),hashMap.get(place2));
        System.out.println("時差是:"+co.ans+"小時");

        System.out.println();
        System.out.println("Input one place:");
        String place3 = input.next();
        System.out.println("經緯度為:"+hashMap.get(place3));
    }


}

class Col{
    int ans;
    public Col(String a, String b) {
        String[]a1 =a.split(",");   //求E
        String[]b1 =b.split(",");

        String[]a2 =a1[1].split("°");   //求度
        String[]b2 =b1[1].split("°");
        ans = ((Integer.valueOf(a2[0]).intValue()-Integer.valueOf(b2[0]).intValue())/15);
    }


}