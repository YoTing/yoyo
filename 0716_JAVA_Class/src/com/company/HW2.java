package com.company;

import java.util.*;

public class HW2 {
    //用ArrayList和LinkList做出堆疊和佇列
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String in = input.next();
        //stack
        ArrayList<String> stack = new ArrayList<>();
        stack.add("1"); stack.add("2"); stack.add("3"); stack.add(in);

        Iterator iterator = stack.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        for (int i=stack.size()-1;i>=0;i--){
            System.out.println(stack.get(i));
        }

        //queue
        System.out.println();
        Queue<String> queue = new LinkedList<>();
        queue.offer("A"); queue.offer("B"); queue.offer("C"); queue.offer(in);

        Iterator it = queue.iterator();
        while (it.hasNext()){
            System.out.print(it.next() + " ");
        }
        System.out.println();
        while (!queue.isEmpty()){
            System.out.println(queue.poll());
        }
    }
}
