package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class HW1 {
    //常整數相加與相減
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        ArrayList<String> num1 = new ArrayList<String>();


        System.out.println("Input two number:");
        num1.add(input.next());
        num1.add(input.next());
        BigNumber n1 =new BigNumber(num1.get(0),num1.get(1));
        System.out.println("相加結果:"+n1.add);
        System.out.println("相減結果:"+n1.less);
    }


}

class BigNumber {

    String add;
    String less;
    BigNumber(String a,String b){

        //add
        if (a.length() > b.length()){
            String t = a;
            a = b;
            b = t;
        }
        int cha = b.length() - a.length();
        for (int i=0; i<cha; i++){
            a="0"+a;
        }
        add = "";
        int w = 0;
        for (int i =b.length()-1; i>=0; i--){
            int c =b.charAt(i)+a.charAt(i)-96+w;
            w =c/10;
            add = (c%10)+add;
        }
        if (w==1){ add = 1+add; }

        //less
        if (a.length() == b.length() && a.compareTo(b)<0 || a.length() <b.length()){
            String t = a;
            a = b;
            b = t;
        }
        for (int i=0; i<Math.abs(a.length()-b.length()); i++){
            b="0"+b;
        }
        less = "";
        int q = 0;
        for (int i =a.length()-1; i>=0; i--){
            int d =a.charAt(i)-b.charAt(i)+q;
            if (d<0){ d+=10; q=-1; }
            else { q=0; }
            less = d+less;
        }
    }


}
