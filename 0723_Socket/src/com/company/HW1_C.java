package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class HW1_C {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        try{
            Socket socket=new Socket(InetAddress.getByName("192.168.43.193"),1226);

            DataInputStream instream=new DataInputStream(socket.getInputStream());
            DataOutputStream outstream=new DataOutputStream(socket.getOutputStream());
            while (true) {
                String serverinput = input.next();
                outstream.writeUTF(serverinput);
                if (serverinput.equals("stop")){
                    System.out.println("DisConnect");
                    break;
                }
                String clientoutput = instream.readUTF();
                System.out.println("Server端訊息:"+clientoutput);
                if (clientoutput.equals("stop")){
                    System.out.println("DisConnect");
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("error");
        }
    }
}
