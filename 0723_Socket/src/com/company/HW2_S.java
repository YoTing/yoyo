package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class HW2_S {
    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(1226);
            System.out.println("Server Created");
            System.out.println("開始傾聽");
            Socket s = server.accept();
            System.out.println("已有客戶端連線" + s.getInetAddress().getHostAddress());
            System.out.println('\t');

            DataInputStream instream = new DataInputStream(s.getInputStream());
            DataOutputStream outstream = new DataOutputStream(s.getOutputStream());

            ArrayList<Integer> range = new ArrayList<>();
            for(int a=1;a<10;a++){
                for(int b=0;b<10;b++){ if(b==a){continue;}
                    for(int c=0;c<10;c++){ if(a==c||b==c){continue;}
                        for(int d=0;d<10;d++){ if(a==d||b==d||c==d){continue;}
                            range.add(a*1000+b*100+c*10+d);

                        }
                    }
                }
            }
            int time = 1;

            int rangenum = (int)(Math.random()*range.size());             //傳送猜的數字
            System.out.println("rangenum:" + rangenum);
            outstream.writeUTF("1");
            outstream.writeUTF("可能的組合有:" + range.size() + "種\n第" + time + "次猜\n電腦猜的數字是:" + range.get(rangenum));
            while (true) {
                try {
                    ArrayList<Integer> change = new ArrayList<>();
                    System.out.println("size:" + range.size());
                    System.out.println("ANS:"+range.get(rangenum));
                    int reA = 0, reB = 0;
                    String result = instream.readUTF();                         //讀取幾A幾B
                    System.out.println(result);
                    String [] re = result.split(",");
                    System.out.println(re[0] + "A" + re[1] + "B") ;
                    reA = Integer.valueOf(re[0]);
                    reB = Integer.valueOf(re[1]);
                    System.out.println("A+B"+(reA+reB));

                    if (re[0].equals("4") && re[1].equals("0")){
                        outstream.writeUTF("2");
                        System.out.println("2");
                        outstream.writeUTF("遊戲結束，電腦已猜中，共猜測" + time + "次");
                        break;
                    }else if ((reA+reB) > 4) {
                        outstream.writeUTF("3");
                        System.out.println("3");
                        outstream.writeUTF("輸入錯誤請重新輸入\n請輸入幾A幾B:");
                        System.out.println("Error:"+range.get(rangenum));
                    }else if (range.size() <= 0){
                        outstream.writeUTF("4");
                        System.out.println("4");
                        outstream.writeUTF("可能組合為0，玩家欺騙電腦，遊戲結束。");
                        break;
                    }else if ((reA+reB) <= 4) {
                        time++;
                        for (int i = 0; i < range.size(); i++) {                                       /*電腦縮小範圍*/
                            int[] ans = {range.get(i) / 1000, range.get(i) / 100 % 10, range.get(i) / 10 % 10, range.get(i) % 10};
                            int[] guess = {range.get(rangenum) / 1000, range.get(rangenum) / 100 % 10, range.get(rangenum) / 10 % 10, range.get(rangenum) % 10};
                            int A = 0, B = 0;
                            for (int j = 0; j < 4; j++) {
                                for (int k = 0; k < 4; k++) {
                                    if (ans[j] == guess[k]) {
                                        if (j == k) {
                                            A++;
                                        } else {
                                            B++;
                                        }
                                    }
                                }
                            }
                            if (A == Integer.valueOf(re[0]) && B == Integer.valueOf(re[1])) {
                                change.add(ans[0] * 1000 + ans[1] * 100 + ans[2] * 10 + ans[3]);
                            }
                        }
                        range = change;
                        rangenum = (int)(Math.random()*range.size());             //傳送猜的數字
                        System.out.println("rangenum:" + rangenum);
                        outstream.writeUTF("1");
                        outstream.writeUTF("可能的組合有:" + range.size() + "種\n第" + time + "次猜\n電腦猜的數字是:" + range.get(rangenum));

                    }
                }catch (IOException e) {
                    System.out.println("error");
                }

            }
        } catch (IOException e) {
            System.out.println("error");
        }
    }
}
