package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class HW3_S {
    static ArrayList<receive> re=new ArrayList();
    static ArrayList<String> Name=new ArrayList<>();
    static ServerSocket server;
    static Socket s;
    public static void main(String[] args){
        DataInputStream instream;

        try {
            server = new ServerSocket(1226);
            System.out.println("Server created");
            System.out.println("開始傾聽");

            System.out.println('\t');


            while (true) {
                s = server.accept();
                instream=new DataInputStream(s.getInputStream());

                String name=instream.readUTF();
                Name.add(name);
                receive r = new receive(name,s);

                re.add(r);
                r.start();

                for (int i=0;i<Name.size();i++){
                    if (Name.get(i).equals(name)){
                        System.out.println(name+"Join!");
                        re.get(i).outstream.writeUTF("Join!");
                    }else{
                        re.get(i).outstream.writeUTF(name+"Join!");
                    }
                }
                System.out.println(Name.size() + "," + Name);
            }
        } catch (IOException e) {
            System.out.println("Error");
        }

    }

    public static class receive extends Thread{         /*收訊息*/

        DataInputStream instream ;
        DataOutputStream outstream ;
        String name;
        Socket socket;
        receive(String n,Socket s) throws IOException {
            name=n; socket=s;
            instream = new DataInputStream(socket.getInputStream());
            outstream = new DataOutputStream(socket.getOutputStream());
        }

        public void run() {
            try {
                DataInputStream instream = new DataInputStream(socket.getInputStream());
                while (true) {
                    String receive = instream.readUTF();
                    String[] restr = receive.split(":");
                    System.out.println("receive:" + receive);
                    System.out.println("restr:" + restr.length);
                    if (restr.length == 1) {
                        if (receive.equals("")) {

                        } else if (receive.equals("exit")) {                  //收到離開訊息
                            for (int i = 0; i < Name.size(); i++) {
                                if (Name.get(i).equals(name)) {
                                    re.get(i).outstream.writeUTF("Leave");
                                    System.out.println(name + " leave");
                                } else {
                                    re.get(i).outstream.writeUTF(name + " leave");
                                }
                            }
                            int postion = Name.indexOf(name);
                            Name.remove(postion);
                            re.remove(postion);
                        } else if (receive.equals("now")) {                    //收到確認線上人數訊息
                            int b=Name.indexOf(name);
                            re.get(b).outstream.writeUTF("NOW");
                            System.out.println(name + " now");
                            re.get(b).outstream.writeUTF("現在線上人數有" + Name.size() + "人\n有:" + Name);

                        } else {
                            for (int i = 0; i < Name.size(); i++) {
                                if (Name.get(i).equals(name)) {
                                    System.out.println(name + " message:" + receive);
                                } else {
                                    re.get(i).outstream.writeUTF(name + " message:" + receive);
                                }
                            }
                        }
                    }else if (restr.length != 1){
                        for (int i = 0; i < Name.size(); i++) {
                            if (Name.get(i).equals(restr[0])) {
                                re.get(i).outstream.writeUTF("PRMSG");
                                re.get(i).outstream.writeUTF(name +" said to you:" + restr[1]);
                                System.out.println(name + " said :" + receive);
                            }
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println("Disconnect");
            }

        }
    }
}
