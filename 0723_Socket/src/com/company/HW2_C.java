package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class HW2_C {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        try{
            Socket socket=new Socket(InetAddress.getByName("192.168.43.193"),1226);

            DataInputStream instream=new DataInputStream(socket.getInputStream());
            DataOutputStream outstream=new DataOutputStream(socket.getOutputStream());

            System.out.println("-----Game Start!-----");
            while (true) {
                String order = instream.readUTF();
                switch (order){
                    case "1":
                        String time = instream.readUTF();           //讀取電腦猜的數字
                        System.out.println(time);

                        System.out.println("請輸入幾A幾B:");
                        String ans = input.next();                  //傳送電腦猜測的結果
                        outstream.writeUTF(ans);
                        break;
                    case "2":                                       //遊戲結束
                        String result = instream.readUTF();
                        System.out.println(result);
                        break;
                    case "3":
                        String error = instream.readUTF();          //輸入錯誤
                        System.out.println(error);
                        ans = input.next();                         //傳送電腦猜測的結果
                        outstream.writeUTF(ans);
                        break;
                    case "4":                                       //欺騙
                        String end = instream.readUTF();
                        System.out.println(end);
                        break;
                }


            }
        } catch (IOException e) {
            System.out.println("DisConnect");
        }
    }
}
