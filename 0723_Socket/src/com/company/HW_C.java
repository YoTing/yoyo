package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class HW_C {
    static Scanner input=new Scanner(System.in);
    static String name;
    static Socket socket;

    public static void main(String[] args) {

        try {
            socket = new Socket(InetAddress.getByName("192.168.43.193"), 1226);
            DataOutputStream dos=new DataOutputStream(socket.getOutputStream());
            send se = new send();
            receive re = new receive();
            System.out.println("Please enter your name:");
            name=input.next();
            dos.writeUTF(name);
            se.start();
            re.start();


        } catch (IOException e) {
            System.out.println("Error");
        }
    }

    public static class send extends Thread{            /*傳訊息*/
        public void run() {
            try {
                DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());
                while (true) {
                    String send ="";
                    send = input.nextLine();
                    outstream.writeUTF(send);
                }
            } catch (IOException e) {
                System.out.println("Disconnect");
            }
        }
    }

    public static class receive extends Thread{         /*收訊息*/
        public void run(){
            try {
                DataInputStream instream = new DataInputStream(socket.getInputStream());
                while (true) {
                    String receive = instream.readUTF();
                    if(receive.equals("Leave")) {
                        System.out.println(receive);
                        break;
                    }else if(receive.equals("NOW") || receive.equals("PRMSG")) {
                        String str = instream.readUTF();
                        System.out.println(str);
                    }else{
                        System.out.println(receive);

                    }
                }
            } catch (IOException e) {
                System.out.println("Disconnect");
            }

        }
    }
}
