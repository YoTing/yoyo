package com.company;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class HW1_S {
    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(1226);
            System.out.println("Server Created");
            System.out.println("開始傾聽");
            Socket s = server.accept();
            System.out.println("已有客戶端連線" + s.getInetAddress().getHostAddress());
            System.out.println('\t');

            Scanner input = new Scanner(System.in);
            DataInputStream instream = new DataInputStream(s.getInputStream());
            DataOutputStream outstream = new DataOutputStream(s.getOutputStream());
            while (true) {
                String clientinput = instream.readUTF();
                System.out.println("Client端訊息:" + clientinput);
                if (clientinput.equals("stop")){
                    System.out.println("DisConnect");
                    break;
                }
                String serveroutput = input.next();
                outstream.writeUTF(serveroutput);
                if (serveroutput.equals("stop")){
                    System.out.println("DisConnect");
                    break;
                }

            }
        } catch (IOException e) {
            System.out.println("error");
        }
    }
}
