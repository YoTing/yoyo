package com.company;

public class HW2 {
    public static void main(String[] args) {
        Animal a =
            new Animal(){
                public void move(){
                    System.out.println("Move:walk");
                }

                public void sound() {
                    System.out.println("Sound:mo~");
                }
            };
        a.setName("MO");
        a.move(); a.sound();
    }

    public static abstract class Animal{
        String name;
        public Animal(){
            name = "Animal";
        }

        public void setName(String n){
            name = n;
            System.out.println("Name:"+name);
        }

        public abstract void move();
        public abstract void sound();
    }


}
