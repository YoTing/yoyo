package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        List<account> arr = Arrays.asList(
            new account("B",30),
            new account("A",78),
            new account("C",66),
            new account("E",1200),
            new account("D",18)
        );

        Collections.sort(arr);
        System.out.println(arr);

        arr.sort( Comparator.<account, String>comparing(p -> p.name) );
        System.out.println(arr);
    }

}

class account implements Comparable<account>{
    public int balance;
    public String name;

    account(String n, int b) {
        this.name = n; this.balance = b;
    }

    @Override
    public String toString() {
        return String.format("account(%s, %d)", name, balance);
    }

    @Override
    public int compareTo(account o) {
        return this.balance - o.balance;
    }
}