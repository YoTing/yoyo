package com.company;

import java.util.Scanner;

public class HW1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        SavingAccount sa = new SavingAccount();
        CheckAccount ca = new CheckAccount();

        System.out.println("Saving or Check...:");
        String order = input.next();
        if (order.equals("saving")) {                           //Saving
            System.out.println("Credit or Debit...:");
            order = input.next();
            if (order.equals("credit")) {                       //credit
                System.out.println("Enter the Amount...:");
                double money = input.nextDouble();
                sa.credit(money);
                sa.setInterestRate();
                System.out.println("Balance:" + sa.balance);
                sa.calculateInterest();
            }else if (order.equals("debit")) {                  //debit
                System.out.println("Enter the Amount...:");
                double money = input.nextDouble();
                sa.debit(money);
                System.out.println("Balance:" + sa.balance);
                sa.calculateInterest();
            }
        }else if (order.equals("check")) {                      //Check
            System.out.println("Credit or Debit...:");
            order = input.next();
            if (order.equals("credit")) {                       //credit
                System.out.println("Enter the Amount...:");
                double money = input.nextDouble();
                ca.chargeFee(money);
                ca.credit();
                System.out.println("Balance:" + ca.balance);
                System.out.println("TransactionFee:" + ca.transactionFee);
            }else if (order.equals("debit")) {                  //debit
                System.out.println("Enter the Amount...:");
                double money = input.nextDouble();
                ca.chargeFee(money);
                ca.debit();
                System.out.println("Balance:" + ca.balance);
                System.out.println("TransactionFee:" + ca.transactionFee);
            }
        }

    }



    public static class Account{
        public double balance = 2000;
        public void credit(double money){
            balance += money;

        }
        public void debit(double money){
            balance -= money;
        }
    }

    public static class SavingAccount extends Account{
        public double interestRate,col;
        public void setInterestRate(){
            interestRate = 0.002;
        }
        public double getInterestRate(){
            return interestRate;
        }
        public void calculateInterest(){
            col = balance*interestRate;
            System.out.println("Rate:" + col);
        }
    }

    public static class CheckAccount extends Account{
        public double transactionFee, m;
        public void chargeFee(double mooey){
            transactionFee = mooey*0.01;
            m = mooey - transactionFee;
        }
        public void credit(){ balance += m; }
        public void debit(){
            balance -= m;
        }
    }
}
