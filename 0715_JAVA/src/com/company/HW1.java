package com.company;

import java.util.Scanner;

public class HW1 {
    //****的迴圈練習
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Input the number:");
        int n = input.nextInt();

        if (n > 0) {
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n - i; j++) {
                    System.out.print(" ");
                }
                for (int k = 1; k <= i; k++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }else if (n <= 0){
            System.out.println("輸入有誤");
        }
    }
}
