package com.company;

import java.util.Scanner;

public class HW3 {
    //宣告一個任意長度的陣列，並用迴圈將陣列的內容反轉
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input the number:");
        int num = input.nextInt();
        int[] n =new int[num];
        for (int i=0; i<num; i++){
            n[i] =(int)(Math.random()*100)+1;
        }

        System.out.println("Your input:");
        for (int i=0; i<num; i++){
            System.out.print(n[i] + " ");
        }
        System.out.println();
        System.out.println("Reversal:");
        for (int i=num-1; i>=0; i--){
            System.out.print(n[i] + " ");
        }
    }
}
