package com.company;

import java.util.Scanner;

public class HW2 {
    //終極密碼
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = (int)(Math.random()*100)+1;
        System.out.println("num:"+num);
        int max = 100, min =0;
        while (true) {
            System.out.println("Input the number:");
            int guess = input.nextInt();
            if (guess <= 100 && guess >= 1) {
                if (guess > num) {
                    max = guess-1;
                    System.out.println("猜太大了。" + min + "~" + max);
                } else if (guess < num) {
                    min = guess+1;
                    System.out.println("猜太小了。" + min + "~" + max);
                } else if (guess == num) {
                    System.out.println("猜中了。");
                    break;
                }
            } else {
                System.out.println("範圍錯誤");
            }
        }
    }
}
